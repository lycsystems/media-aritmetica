
import java.util.Scanner;

public class CalcularMedia {

    public static void main(String[] args) {

    Scanner sc = new Scanner(System.in);
    
    int Indice=0;
    int Suma=0;
    int Numero;

    int ArrayNumeros[] = new int[10];

    for (Indice=0; Indice<10; Indice++)
        {

            System.out.print("Introduce el valor número "+(Indice+1)+": ");
            Numero = sc.nextInt();
                
            ArrayNumeros[Indice]=Numero;
            
            Suma=Suma+Numero;

            System.out.println("La media hasta ahora es: "+(Suma/(Indice+1)));

        }
    }
    
}
